; Copy CP/M from live system to tracks 0 and 1 of floppy drive a
; njm Apr 2021

	org 0x100
; read memory from DC00 to FFFF and write to tracks 0 and 1
; This is 9216. bytes = 72 sectors
; Should fit entirely on track 0 :)
	ld	sp,0xffff

	ld	HL,dmaBuffer	; address of buffer
	ld	(dmaad),HL	; save this

	ld	a,0
	ld	(diskno),a
	ld	(track),a
	ld	(sector),a	; initial disk/track/sector
	ld	a,72		; total # of tracks to save
	ld	(secCount),a	; countdown
	ld	HL,0xDC00	; initial address to save

; main save loop
saveLoop:
	ld	a,0x80		; read 128 bytes
	ld	d,a		; d is out countdown timer
	call	doCopy		; copy block into dma buffer
	call	WRITE		; and write to floppy
; signal console
	ld	a,(secCount)
	add	a,0x21
	ld	c,a
	call	CONOUT

	ld	bc,0x80
	add	hl,bc		; next location to read from
	ld	a,(sector)
	inc	a
	ld	(sector),a	; next sector
	ld	a,(secCount)
	dec	a		; # tracks left
	ld	(secCount),a
	jp	nz,saveLoop

; done!
	ld	c,'!'
	call	CONOUT
	halt


; copy 128 bytes from (HL) to dmaBuffer
doCopy:
	push	HL		; save this
	ld	a,0x80
	ld	c,a		; byte counter
	ld	DE,(dmaad)	; where to save
doCopyLoop:
	ld	a,(HL)
	ld	(DE),a		; copy byte
	inc	HL
	inc	DE		; move forward :)
	dec	c
	jp	nz, doCopyLoop	; do 128x
	pop	HL		; restore
	ret

send:
        out (0x82),a    ; send to host
; now wait for host to ingest and respond
sendLoop:
        in a,(0x81)     ; check DATA_AVAIL
        and 1           ; <D0>  
        jp nz,sendLoop  ; if set, hang out here
; response ready
; wait for a bit; otherwise host may still be asserting clear signal
        call sleep
; now read response
        in a,(0x82)
        ret

sleep:
        ld a,8
sleepLoop:
        dec a
        jp nz, sleepLoop
        ret

CONST:
	ld a,0x01
	call send
	and 1
	jp z,notReady
	ld a,0xff
	ret
notReady:
	ret

CONIN:
	ld a,0x02
	call send
	ret

CONOUT:
	ld a,0x03
	call send
	ld a,c
	call send
	ret

READ:
	push BC
	push DE
	push HL	; save these

	ld HL,(dmaad)	; start saving here
	ld a,0x80	; read 128 bytes
	ld d,a		; d is out countdown timer

	ld a,0x04
	call send
	ld a,(diskno)
	call send
	ld a,(track)
	call send
	ld a,(sector)
	call send

; a has first byte...do the DMA thing
ReadDMALoop:
	ld (HL),a	; save this byte
	inc HL		; next save loc
	dec d		; countdown
	jp z, ReadDMADone	; finished
			; else ask for next byte
	call send	; byte value is ignored by host
	jp ReadDMALoop	; and get more

; all 128 bytes of sector received and loaded into memory
ReadDMADone:
	pop HL
	pop DE
	pop BC		; restore these

	ld a,0 		; success
	ret


WRITE:
	push BC
	push DE
	push HL	; save these

	ld HL,(dmaad)	; start saving here
	ld a,0x80	; read 128 bytes
	ld d,a		; d is out countdown timer

	ld a,0x05
	call send
	ld a,(diskno)
	call send
	ld a,(track)
	call send
	ld a,(sector)
	call send

WriteDMALoop:
	ld a,(HL)	; next byte to send
	call send	; Send it :)
	inc HL		; next read loc
	dec d		; countdown
	jp z, WriteDMADone	; finished
			; else do more
	jp WriteDMALoop

; all 128 bytes of sector read from memory and sent to host
WriteDMADone:
	pop HL
	pop DE
	pop BC		; restore these

	ld a,0 ; success
	ret

diskno	defw	2
track	defw	2
sector	defw	2
secCount defw	2
dmaad	defw	2
dmaBuffer	defw	128

	end
