/*
 * General interface to Z80
 * NJM 30 Mar 2021
 *
 * Everything is driven form the Z80 side
 *
 * Probably best to start this BEFORE Z80 starts running...
 *
 * Z80: Write Data (OUT 0x82)
 *              AT: Wait for DATA_AVAIL
 *              AT: Read data
 *              AT: Write response
 *              AT: Clear DATA_AVAIL
 * Z80: Wait till DATA_AVAIL cleared
 *      (IN 0x81)
 * Z80: Read response (IN 0x82)
 *
 *
 * Wait for DOUTPEND=0
 * Send bits to D3 (MSb first), shift in on rising edge of D2
 * When done, pull D4 LOW to set flag
 *
 * Z80: Read flag on D0 of port 0xFD
 *      When ready, read char on port 0xFE
 *
 */

#include <SPI.h>
#include <SD.h> // SD card access

// interface to Z80
#define CLK 2 // shift on rising edge
#define DIN 7 // read data from here
#define DOUT 4 // write data here
#define CLR_DATA_AVAIL 5 // pull LOW to clear
#define DATA_AVAIL 6 // shows data is available (!)

char sbuf[128]; // sector :)

void setup() {
  Serial.begin(115200); // do tings...
  if (!SD.begin(9)){ // error
    Serial.println("SD Card init error");
    while(1);
  }

  Serial.println("Floppy drives ready");

// set directions
  pinMode(CLK,OUTPUT);
  pinMode(DIN,INPUT);
  pinMode(DOUT,OUTPUT);
  pinMode(CLR_DATA_AVAIL,OUTPUT);
  pinMode(DATA_AVAIL,INPUT);

// initial values
  digitalWrite(CLK,LOW); // pulse high to shift a bit in/out
  digitalWrite(CLR_DATA_AVAIL,LOW);digitalWrite(CLR_DATA_AVAIL,HIGH); // pull low to clear
}


void loop() {
// look for input from KB
  processInput();

// see if Z80 wants something
  if (dataAvailable()){
    int data=readData();
    processData(data);
  }
}

  /////////////////////////////////////////////////////////////////
 // Terminal Input Code (TIC lol cuz GHA (gotta have acronyms)) //
/////////////////////////////////////////////////////////////////

// save incoming chars for possible eventual transmission
char buffer[120];
int bufLen=0;  // # of chars buffered
void processInput()
{
// look for input from KB
  if (Serial.available()){ // KB char ready
    char c=Serial.read(); // get char
    if (bufLen < 32){
      buffer[bufLen++]=c; 
    } else {
      Serial.println("Input buffer overrun :(");
    }
  }
}

// read a char from input buffer
int nextChar() // return next char from input buffer, or -1 at EOF
{
// block until char is available
  if (bufLen==0){
    while (!Serial.available()); // wait till char available
    buffer[0]=Serial.read(); // save in buffer
    bufLen=1; // and record its presence
  }

  //if (bufLen==0) return(-1); // EOF

  int retVal=buffer[0]; // return this at the end

// shift buffer
  for (int i=0;i<bufLen-1;i++) buffer[i]=buffer[i+1];
  bufLen--;

  return(retVal);
}

// is a character available?
int charAvailable() // 1=yes, 0=no
{
  return((bufLen==0)?0:1);
}

  ////////////////////////
 // Z80 interface code //
////////////////////////

int dataAvailable()
{
  return ((digitalRead(DATA_AVAIL)==HIGH)?1:0);
}

// send a byte (response) to Z80
void writeData(int data)
{
// shift in the bits
  for (int i=7;i>=0;i--){
    digitalWrite(DOUT,((1<<i)&data)?HIGH:LOW);
    digitalWrite(CLK,HIGH);digitalWrite(CLK,LOW);
  }

// since RCLK is tied to SRCLK, we need one more pulse to copy the (correct)
// shifted value into the output locations...
  digitalWrite(CLK,HIGH);digitalWrite(CLK,LOW);

// and finally, signal that response is ready
  digitalWrite(CLR_DATA_AVAIL,LOW);digitalWrite(CLR_DATA_AVAIL,HIGH);
// %%% THIS should be edge-triggered...Z80 might set flag while we're still clearing it
}

// read byte from Z80
int readData()
{
  int data=0;
  for (int i=7;i>=0;i--){
    if (digitalRead(DIN)) data=data|(1<<i); // set bit
    digitalWrite(CLK,HIGH);digitalWrite(CLK,LOW); // shift
  }
  return(data);
}

// in the middle of a command dialog, we may be expecting another byte...use this :)
int blockingReadData()
{
  while (!dataAvailable()); // spin here till Z80 sends next part of message
  int c=readData(); // this is char from Z80
  return(c);
}


   /////////////////////////////
  // Higher-Level processing //
 //   Here's the goods :)   //
/////////////////////////////

int lastChar=0;
void processData(int command)
{

  long diskno,track,sector;
  int c;
  long address;
  int data;

  char fname[16]; // temp
  File fp;

//Serial.println(command);

  switch(command){
    case 0x01: // check char ready status
      if (charAvailable()){
        writeData(1);
      } else {
        writeData(0);
      }
      break;

// Sometimes CP/M asks for a character before checking to see if one is available
// In that case, we'll block until a character is available
    case 0x02: // send char to Z80
      writeData(nextChar()); // nextChar() blocks if nothing is available
      break;

    case 0x03: // read char from Z80
      writeData(0); // just to clear flag to indicate receipt of instruction
      c=blockingReadData(); // this is char from Z80
      writeData(0); // Always need to write after read (handshake with Z80)
      Serial.print((char) c);
      break;

    case 0x04: // read disk sector
      writeData(0); // just to clear flag to indicate receipt of instruction
      diskno=blockingReadData(); writeData(0);
      track=blockingReadData(); writeData(0);
      sector=blockingReadData();
// read given sector
      sprintf(fname,"%c/%02d",'a'+(char)diskno,(int)track);
      fp=SD.open(fname,O_READ | O_WRITE);
      address=128*sector; // offset in track file
      fp.seek(address);
      fp.read(sbuf,128);
      fp.close();

      writeData(sbuf[0]); // respond with first byte of data
// now rcv/send 127 more times
      for (int i=1;i<128;i++){
        blockingReadData();
        writeData(sbuf[i]); // send bytes to Z80
      }
      break;

    case 0x05: // write disk sector
      writeData(0); // just to clear flag to indicate receipt of instruction
      diskno=blockingReadData(); writeData(0);
      track=blockingReadData(); writeData(0);
      sector=blockingReadData(); writeData(0);

// now rcv/send 128 times
      for (int i=0;i<128;i++){
        int data=blockingReadData(); // next byte to write to disk
        sbuf[i]=(char)data;
        writeData(0);
      }

// sbuf[] now has updated data
      sprintf(fname,"%c/%02d",'a'+(char)diskno,(int)track);
      fp=SD.open(fname,O_READ | O_WRITE);
      address=128*sector; // offset in track file
      fp.seek(address);
      fp.write(sbuf,128);
      fp.close();
      break;

    default: Serial.print("Unknown command: ");Serial.println(command,HEX);
             writeData(0); // to clear the DATA_AVAIL flag
  }
}
