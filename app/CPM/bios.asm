; %%% raw interface board code

send:
        out (0x82),a    ; send to host
; now wait for host to ingest and respond
sendLoop:
        in a,(0x81)     ; check DATA_AVAIL
        and 1           ; <D0>  
        jp nz,sendLoop  ; if set, hang out here
; response ready

;***
;*** this can be dropped once the signaling is changed to edge-triggered
;***
        call sleep ; wait for a bit; otherwise host may still be asserting clear signal

; now read response
        in a,(0x82)
        ret

sleep:
        ld a,8
sleepLoop:
        dec a
        jp nz, sleepLoop
        ret

; a few of the BIOS routines

CONST:	; console status. Return a=0xff (ready) or 0 (not ready)
	ld a,0x01
	call send
	and 1
	jp z,notReady
	ld a,0xff
	ret
notReady:
	ld a,0x00
	ret

CONIN:	; read char from console into a
	ld a,0x02
	call send
	ret


CONOUT:	; write reg c to console
	ld a,0x03
	call send
	ld a,c
	call send
	ret

; read one sector. Assumes disk, track, sector and dma address have been set
READ:
	push BC
	push DE
	push HL	; save these

	ld HL,(dmaad)	; start saving here
	ld a,0x80	; read 128 bytes
	ld d,a		; d is out countdown timer

	ld a,0x04
	call send
	ld a,(diskno)
	call send
	ld a,(track)
	call send
	ld a,(sector)
	call send

; a has first byte...do the DMA thing
ReadDMALoop:
	ld (HL),a	; save this byte
	inc HL		; next save loc
	dec d		; countdown
	jp z, ReadDMADone	; finished
			; else ask for next byte
	call send	; byte value is ignored by host
	jp ReadDMALoop	; and get more

; all 128 bytes of sector received and loaded into memory
ReadDMADone:
	pop HL
	pop DE
	pop BC		; restore these

	ld a,0 		; success
	ret


WRITE:
; similar to read :)
	push BC
	push DE
	push HL	; save these

	ld HL,(dmaad)	; start saving here
	ld a,0x80	; read 128 bytes
	ld d,a		; d is out countdown timer

	ld a,0x05
	call send
	ld a,(diskno)
	call send
	ld a,(track)
	call send
	ld a,(sector)
	call send

WriteDMALoop:
	ld a,(HL)	; next byte to send
	call send	; Send it :)
	inc HL		; next read loc
	dec d		; countdown
	jp z, WriteDMADone	; finished
			; else do more
	jp WriteDMALoop

; all 128 bytes of sector read from memory and sent to host
WriteDMADone:
	pop HL
	pop DE
	pop BC		; restore these

	ld a,0 ; success
	ret

