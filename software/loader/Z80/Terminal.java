package Z80;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import java.awt.BorderLayout;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JScrollBar;
import javax.swing.text.DefaultCaret;
import java.awt.Color;
import java.awt.event.MouseWheelListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Terminal extends JFrame implements Runnable{
	JTextArea termWindow;
	Loader parent;
	boolean doPause=false; // set when we should exit
	int fontStyle=Font.PLAIN;
        JScrollPane scrollPane;
	
	void askForPause()
	{
	  doPause=true; // we'll exit as soon as possible...
	}

	public Terminal(Loader parent) {
		setVisible(true);
		setSize(640,480);
		this.parent=parent; // save our caller, so we can use their serialSend() etc.
		
		termWindow = new JTextArea(24,80);
//%%% this makes things work well on windows
//%%% but breaks it here
		DefaultCaret caret=(DefaultCaret)termWindow.getCaret();
		caret.setUpdatePolicy(DefaultCaret.NEVER_UPDATE);

		termWindow.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			  //System.out.println("e="+e);
			   if (e.getButton()==2){
			     fontStyle=(fontStyle==Font.PLAIN)?Font.BOLD:Font.PLAIN;
			     adjustFont(0); // apply the new style
			   }
			}
		});
		termWindow.addMouseWheelListener(new MouseWheelListener() {
			public void mouseWheelMoved(MouseWheelEvent e) {
			  if ((e.getModifiers() & InputEvent.CTRL_MASK) != 0){ // ctrl + scrollwheel
			    adjustFont((e.getWheelRotation() > 0)?-1:1);
			  }
			}
		});
		termWindow.setLineWrap(true);
		termWindow.setCaretColor(Color.GREEN);
		termWindow.setSelectedTextColor(Color.CYAN);
		termWindow.setForeground(Color.GREEN);
		termWindow.setBackground(Color.BLACK);
		termWindow.setEditable(false);
	    termWindow.getCaret().setVisible(true);
	    adjustFont(0);
		
		termWindow.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
			    char c=e.getKeyChar();
			    if (c=='\n') c='\r'; // \n confuses Arduino or serial program or something? :/
				parent.serialSend(""+c);
			}
		});
		//getContentPane().add(termWindow, BorderLayout.CENTER);
		scrollPane=new JScrollPane(termWindow);
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		//scrollPane.add(termWindow);
	}

	@Override
	public void run() {
	  while (true){
	    byte b=parent.getChar();
	    char c=(char)(b&0xff); // ASCII-centric :/

	    String curr=termWindow.getText(); // this blocks

// should we pause for a bit?
	    if (doPause){ // pause until we're resumed
	      doPause=false; // clear this flag
	      boolean wakeUp=false;
	      while (!wakeUp){
	        try {
				Thread.sleep(10000);
			} catch (InterruptedException e) { // wake up!
				//curr=curr+"\nFinished loading file"; // good-morning :)
				c='\n';
				wakeUp=true;
			}
	      }
	    } // we slept, and now we're back to work
	    
		curr=curr+c;
		if (curr.length() > 100000) curr=curr.substring(curr.length()-10000);
	    termWindow.setText(curr);
            //JScrollBar vertical=scrollPane.getVerticalScrollBar();
            //vertical.setValue(vertical.getMaximum());
	    termWindow.setCaretPosition(curr.length());
	  }
	}
	
	int font=16;
	
	void adjustFont(int inc)
	{
	  font+=inc;
	  if (font > 45) font=45;
	  if (font < 6) font=6;
      //termWindow.setFont(new Font("L M Mono10", fontStyle, font));
      termWindow.setFont(new Font("Liberation Mono", fontStyle, font));
	}
}
