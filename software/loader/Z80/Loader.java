package Z80;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.awt.event.ActionEvent;

import com.fazecast.jSerialComm.*;
import javax.swing.JTextField;
import javax.swing.JTextArea;

public class Loader extends JFrame {

  private JPanel contentPane;
  
  File theFile=null; // file to read
  JLabel lblFilename = new JLabel("(selected filename)");
  JFileChooser chooser = new JFileChooser();
  JTextArea portList;
  SerialPort[] allPorts;
  JTextField portNum;
  JTextArea messages;
  JButton btnLoad;
  boolean portOpen=false;
  boolean fileSelected=false; // need both of these to activate Load button
  Loader me=this;
  Terminal terminal; // make/start/ask to pause/resume/ask to pause/resume/etc.
  Thread terminalThread;

  /**
   * Launch the application.
   */
  public static void main(String[] args) {
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        try {
          Loader frame = new Loader();
          frame.setVisible(true);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });
  }

  /**
   * Create the frame.
   */
  public Loader() {
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setBounds(100, 100, 612, 463);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);
    contentPane.setLayout(null);
    
    lblFilename.setBounds(40, 13, 529, 15);
    contentPane.add(lblFilename);
    
// open file, read and send to Z80 using special LOAD command
    btnLoad = new JButton("Load");
    btnLoad.setEnabled(false);
    btnLoad.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        btnLoad.setEnabled(false);
        doLoad();
        btnLoad.setEnabled(true);
      }
    });
    btnLoad.setBounds(40, 88, 117, 25);
    contentPane.add(btnLoad);
    
    JButton btnChooseFile = new JButton("Choose File");
    btnChooseFile.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        chooseFile();
      }
    });
    btnChooseFile.setBounds(40, 40, 117, 25);
    contentPane.add(btnChooseFile);
    
    JButton btnExit = new JButton("Exit");
    btnExit.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        System.exit(0);
      }
    });
    btnExit.setBounds(444, 387, 117, 25);
    contentPane.add(btnExit);
    
    portNum = new JTextField();
    portNum.setText("0");
    portNum.setBounds(270, 375, 114, 19);
    contentPane.add(portNum);
    portNum.setColumns(10);
    
    JLabel lblPortNumber = new JLabel("Port Number:");
    lblPortNumber.setBounds(146, 377, 117, 15);
    contentPane.add(lblPortNumber);
    
    portList = new JTextArea();
    portList.setEditable(false);
    portList.setBounds(270, 70, 294, 296);
    contentPane.add(portList);
    
    // user is selecting a serial port
    JButton btnOpen = new JButton("Open");
    btnOpen.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        int portIndex;
        try {
          portIndex=Integer.parseInt(portNum.getText());
        } catch (Exception err) {
          messages.setText(""+err);
          return;
        }
        if ((portIndex < 0) || (portIndex > allPorts.length)) { // last index is legal :)
          messages.setText("Illegal port number");
          return;
        }
        if (portIndex == allPorts.length) { // wifi
        	serialOpen(false);
        } else {
          port=allPorts[portIndex];
          serialOpen(true);
        }

        btnOpen.setEnabled(false); // only one shot!

		startTerminal();
      }
    });
    btnOpen.setBounds(267, 399, 117, 25);
    contentPane.add(btnOpen);
    
    messages = new JTextArea();
    messages.setWrapStyleWord(true);
    messages.setLineWrap(true);
    messages.setText("Enter a port# and press Open...");
    messages.setEditable(false);
    messages.setBounds(29, 158, 213, 154);
    contentPane.add(messages);
    
    JLabel lblAvailablePorts = new JLabel("Available Ports:");
    lblAvailablePorts.setBounds(320, 45, 155, 15);
    contentPane.add(lblAvailablePorts);
    //portNum.requestFocusInWindow();
    serialSetup(); // display list of devices
  }
    
// let user pick filename
  public void chooseFile()
  {
      FileNameExtensionFilter filter = new FileNameExtensionFilter(
          "Intel Hex File", "hex");
      chooser.setFileFilter(filter);
      int returnVal=chooser.showOpenDialog(this);
      if(returnVal==JFileChooser.APPROVE_OPTION) {
         lblFilename.setText(chooser.getSelectedFile().getPath());
      }
      theFile=chooser.getSelectedFile();
      chooser.setCurrentDirectory(theFile);
      messages.setText(theFile.getPath() + " is open for reading");
      if (portOpen) btnLoad.setEnabled(true);;
  }
  
// display port list
  void serialSetup()
  {
    // find, save and display list of all serial ports
    allPorts=SerialPort.getCommPorts();
    for (int i=0;i<allPorts.length;i++) {
      portList.setText(portList.getText() + i + ":" + allPorts[i] + "\n");
      //System.out.println("Port " + i + ": " + allPorts[i]);
    }
    portList.setText(portList.getText()+ allPorts.length + ": [WiFi->88:8081]");
  }

  boolean serial=true;
  SerialPort port; // talk on this!
  InputStream wiPort; // or this :)
  OutputStream woPort; // (and this)
  
  void serialOpen(boolean ser)
  {
    serial=ser; // true for actual serial port; false for wifi
    if (!serial) {
    	wifiOpen();
    	return;
    }

    // do serial port here
    System.out.println("Opening " + port);
    try {
      port.openPort();
      port.setBaudRate(115200);
      port.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING|SerialPort.TIMEOUT_WRITE_BLOCKING, 0, 0); // All I/O is blocking
      messages.setText("Port open");
      portOpen=true;
      if (theFile != null) btnLoad.setEnabled(true);
    } catch (Exception err){
      messages.setText(""+err);
      portOpen=false;
    }
  }
  
  Socket sock;
  // open connection to socket 8081
  void wifiOpen()
  {
    System.out.println("Opening WiFi:");
	  try {
		sock=new Socket("192.168.0.88",8081);
System.out.println("sock=" + sock);
		wiPort=sock.getInputStream();
System.out.println("wiPort=" + wiPort);
		woPort=sock.getOutputStream();
System.out.println("woPort=" + woPort);
		messages.setText("WiFi connection established");
		portOpen=true;
		return;
	} catch (Exception e) {
System.out.println("Error opening wifi: " + e);
		messages.setText("Can't open WiFi connection to 192.168.0.88 (port 8081): " + e);
		e.printStackTrace();
		portOpen=false;
		return;
	}
	  
  }

// Serial I/O code (both USB and WiFi, based on "serial" flag)

  byte[] buffer=new byte[120];

  void serialSend(String s)
  {
    buffer=s.getBytes();
    if (serial) {
      port.writeBytes(buffer, buffer.length);
    } else { // WiFi
    	try {
			woPort.write(buffer,0,buffer.length);
		} catch (IOException e) {
			messages.setText("Something went wrong with WiFi send: " + e);
			e.printStackTrace();
		}
    }
  }
  
  // read anything available from serial port (non-blocking)
  void serialRcv()
  {
    if (serial) {
      while (port.bytesAvailable() > 0){
        port.readBytes(buffer, 1);
        //System.out.print((char)buffer[0]);
      }
      return;
    } else { // WiFi
    	try {
    	  while (wiPort.available() > 0){
    	    wiPort.read();
    	  }
    	} catch (Exception e) {
			messages.setText("Something went wrong; " + e);
			e.printStackTrace();
    	}
    }
  }
  
  // read one char from serial port (blocking)
  byte getChar()
  {
    if (serial) { // USB section
      port.readBytes(buffer, 1);
      return(buffer[0]);
    }

// WiFi section
  	try {
      buffer[0]=(byte)wiPort.read();
      return(buffer[0]);
    } catch (Exception e) {
	  messages.setText("Something went wrong; " + e);
	  e.printStackTrace();
	  return('x');
    }
  }
  
  byte getAck()
  {
    if (serial) { // USB section
      port.readBytes(buffer, 1);
      while (buffer[0] != '.') port.readBytes(buffer,1); // spin until ack ('.')
      return(buffer[0]);
    }

// WiFi section
  	try {
      buffer[0]=(byte)wiPort.read();
      while (buffer[0] != '.') buffer[0]=(byte)wiPort.read(); // spin until ack ('.')
      return(buffer[0]);
    } catch (Exception e) {
	  messages.setText("Something went wrong; " + e);
	  e.printStackTrace();
	  return('x');
    }
  }
  
  void hexError()   // some sort of input file format error
  {
    messages.setText("Input file format error");
  }
  
  void startLoad() // send load command, flush echo, etc.
  {
    serialSend("load\n");
    getAck();
  }
  
  // read file and send to Z80 board
  void doLoad()
  {
    int count=0; // # of bytes sent

  	pauseTerminal(); // ask terminal to stop eating input from Z80

    // ingest the hex file
    try {
      int b;
      Scanner sc=new Scanner(theFile);
      while (sc.hasNextLine()){
        String buffer=sc.nextLine();

// Look for ":" in start of line
        if (buffer.charAt(0) != ':'){
          hexError();sc.close();return;
        }
System.out.println("<"+buffer+">");
        int numBytes=Integer.parseInt(buffer.substring(1,3),16); // : <numbytes> <starting addr> <flag> <data...>
        count+=numBytes; // running tally
        int startingAddress=Integer.parseInt(buffer.substring(3,7),16);
// sending load command for 0 bytes will cause EOL flash on Z80 board

        startLoad(); // send load command
        serialSend(String.format("%02x",numBytes)); // plus number of bytes to receive
        serialSend(String.format("%04X", startingAddress)); // followed by address
        getAck(); // get ack

        for (int byteNum=0;byteNum<numBytes;byteNum++){
          int byteToSend=Integer.parseInt(buffer.substring(9+2*byteNum,9+2*byteNum+2),16);
          serialSend(String.format("%02x", byteToSend)); // send data to load
        }
        getAck(); // ack after entire line

      } // end of file
      sc.close();
    } catch (Exception e1) {
      messages.setText("Error opening file:\n"+e1);
      e1.printStackTrace();
      return;
    }
    
    System.out.println("\nLoad finished");
    messages.setText("File loaded. Size=" + count + " byte" + (count==1?"":"s"));
	resumeTerminal();
  }

  // legacy code - for z80-asm style binary files
  void doLoadOld()
  {
  	pauseTerminal(); // ask terminal to stop eating input from Z80
    serialSend("load\n"); // terminal window will eat first character, then go quiet
    // to flush the buffer, let's wait till we have one char, and then do a non-blocking read loop
    getAck();getAck();getAck();getAck();
    serialRcv(); //flush the rest of the buffer
    
    // ingest the binary file
    try {
      int b;
      InputStream bis=new FileInputStream(theFile);
// check header
      byte[] header=new byte[8];
      bis.read(header,0,6); // read the (partial) header
      String head=new String(header,0,6);
      if (!head.equals("Z80ASM")) {
        messages.setText("<"+head+">"+"is not a valid Z80 bin file header");
        bis.close();
        return;
      }
      bis.read(header,0,2); // skip ^Z and \n

// read starting address
      int addr=bis.read();
      addr=(addr<<8) | bis.read();
      serialSend(String.format("%04X", addr));
      getAck(); // this is our ACK (%%% check to make sure it's a '.'?)

// read and send bytes of data
      int count=0;
      while (-1 != (b=bis.read())) {
        serialSend(String.format("%02X", b));
        getAck();
        ++count;
      }

// end load
      serialSend("ZZ"); // causes sscanf to fail in ATMEGA :)
      getAck();
          
      bis.close();
      System.out.println("\nLoad finished");
      messages.setText("File loaded. Size=" + count + " byte" + (count==1?"":"s"));
	  resumeTerminal();
    } catch (Exception e1) {
      messages.setText("Error opening file:\n"+e1);
      e1.printStackTrace();
      return;
    }
  }
  
  // Terminal window manipulation...

// open terminal window
  void startTerminal()
  {
	terminal=new Terminal(me);
	terminalThread=new Thread(terminal); // use this to start and wake
	terminalThread.start(); // start the thread
  }

// ask terminal to exit
  void pauseTerminal()
  {
    terminal.askForPause();
  }
  
  // have terminal resume
  void resumeTerminal()
  {
    terminalThread.interrupt();
  }
}
